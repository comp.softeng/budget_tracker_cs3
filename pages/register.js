import {useState, useEffect} from 'react'
import {Form,Button, Jumbotron} from 'react-bootstrap'
import Router from 'next/router'
//import Swal
import Swal from 'sweetalert2'
import AppHelper from '../app-helper'
import Head from 'next/head'
//
export default function Register(){
    /*What do we bind to our input to track user input in real time?*/
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState(0)
	const [password1,setPassword1] = useState("")
	const [password2,setPassword2] = useState("")

	//stare for conditional rendering for the submit button
    const [isActive, setIsActive] = useState(true)
	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName,lastName,email, mobileNo, password1, password2]) 
    //lets now create our register method
    function registerUser(e) {
       e.preventDefault() //to avoid page redirection. 
       //lets check if an email exists in our records.
       fetch(`${AppHelper.API_URL}/users/email-exists`, {
 	         method: 'POST',
 	         headers: {
 	         	'Content-Type': 'application/json'
 	         },
 	         body: JSON.stringify({
 	         	email: email
 	         }) 
       }).then(res => res.json()).then(data => {
       	  if(data === false){
       	  	fetch(`${AppHelper.API_URL}/users/register`, {
       	  		method: 'POST',
       	  		headers: {
       	  			'Content-Type': 'application/json'
       	  		},
       	  		body: JSON.stringify({
       	  			firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo,
					password: password1
       	  		})
       	  	}).then(res => res.json()).then(data => {
       	  		//you can create a checker here if you wish
       	  		if(data === true) {
       	  			Swal.fire({
       	  				icon: "success",
       	  				title: "Welcome, Wise Person!",
       	  				text: "Thank you for registering.",
						background: '#020025'
       	  			})
       	  			//after displaying a success message redirect the user to the login page
       	  			Router.push('/')
       	  		}else{
       	  			Swal.fire({
       	  				icon: "error",
       	  				title: "Registration failed",
       	  				text: "Something went wrong",
						background: '#020025'
       	  			})
       	  		}
       	  	})
       	  } else {
       	  	//the else branch will run if the return value is true
       	  	Swal.fire({
       	  		icon: 'error',
       	  		title: 'Registration Failed',
       	  		text: 'Email is already taken by someone else, Move on!',
				background: '#020025'
       	  	})
       	  }
       })
    } 
   return (
		<>
		<Head>
				<title> Money Wise | Register</title>
		</Head>
		<h1 className="mt-5 pt-3 text-center">Register</h1>
			<h6 className='register-banner'>Are you a person who lives on a budget? Track your expense by using Money Wise Application and be WISE!</h6>
			<Form onSubmit={e => registerUser(e)} className="mb-3">
				<Form.Group controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control className='form' type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control className='form' type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control className='form' type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control className='form' type="number" placeholder="Enter Mobile No." value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control className='form' type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control className='form' type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
				</Form.Group>

				{
					isActive ?
				<Button className ='btn'  type="submit" >Sign Up</Button>
					:
				<Button className ='btn'  disabled >Sign Up</Button>
			}
			</Form>
		</>
		) 
}
