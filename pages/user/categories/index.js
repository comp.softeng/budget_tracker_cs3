import { useState, useEffect } from 'react'
import { Table, Button, Card, Row, Col } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper'


export default () => {
    const [categories, setCategories] = useState([])
    useEffect(() => {
        const payload = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }
    }
    fetch(`${AppHelper.API_URL}/users/get-category-view`,payload)
    .then(res => res.json()).then(data => {
        setCategories(data)
        })
    }, [])
    return (
        <View title="Money Wise | Categories">
            <h3>Categories</h3>
            <Link href="/user/categories/new"><a className="btn btn-success mt-1 mb-3">Add</a></Link>
            <Table striped bordered hover className='tableColor'>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                        {categories.map((category) => {
                        return (
                                <tr>
                                    <td>
                                        { category.name }
                                    </td>
                                    <td>
                                        { category.type }
                                    </td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </View>

    )
}
